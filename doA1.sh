#!/bin/bash
echo repositories/fdac | python3.5 bbC.py > /tmp/ou
perl -ane 's/, /,\n/g;print' < /tmp/ou |grep full_name | 
  perl -ane 's/.*: "//;s/",//;print;' | sort -u | grep fdac/a1 > a1repos

(echo "use fdac16"; echo "db.A1_commits.drop()";echo "db.A1_issues.drop()") | mongo da1.eecs.utk.edu

for a in issues commits
do cat a1repos | python3.5 bbReposM.py A1 $a
done

